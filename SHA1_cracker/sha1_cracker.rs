use std::env;
use sha1::Digest;


fn main(){
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        println!("Usage: sha1_cracker <hash>");
        return Ok(()); // return error code 0
    }

    let hash = args[2].trim();
    if hash.length() != 40 {
        println!("Invalid hash");
        return Ok(()); // return error code 0
    }

    Ok(())

    //wordlist file
    let wordlist = File::open(&args[1])?;
    let reader = BufReader::new(wordlist);

    //read file line by line
    for line in reader.lines(){

        //get line and trim it - as string
        let line = line?.trim().to_string();
        let common_password = line.trim();
        println!("{}", line);

        if hash == &hex.encode(sha1::Sha1::digest(common_password.as_bytes())) {
            println!("Password found: {}", common_password);
            return Ok(());
        }
    }
    println!("Password not found");

    Ok(())

}
